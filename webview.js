'use strict';

module.exports = Franz => {
  const getMessages = function getMessages() {
    let count = 0;

    if (document.getElementsByClassName('conversation-title mdl-card__supporting-text bold').length > 0) {
      count = document.getElementsByClassName('conversation-title mdl-card__supporting-text bold').length;
    }

    // set Franz badge
    Franz.setBadge(count);
  };

  // check for new messages every second and update Franz badge
  Franz.loop(getMessages);
};
